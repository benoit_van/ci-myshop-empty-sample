package com.shop.starter.controller;

import java.util.HashMap;
import java.util.Map;

import com.shop.starter.model.Item;

public class Storage {
	
private Map<String , Item> itemMap;
	
	public Storage() {
		super();
		this.itemMap = new HashMap<String, Item>();
	}
	
	
	public void addItem(Item obj) {
		this.itemMap.put(obj.getName(), obj);
	}

	public Item getItem(String name) {
		Item obj;
		obj = this.itemMap.get(name);
		return obj;
	}


}
