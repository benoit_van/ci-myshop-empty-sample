package com.shop.starter.controller;

import com.shop.starter.model.Item;

public class Shop {

	
	public float getCash() {
		return cash;
	}

	public void setCash(float cash) {
		this.cash = cash;
	}

	public Shop(Storage storage, float cash) {
		super();
		this.storage = storage;
		this.cash = cash;
	}

	private Storage storage;
	private float cash;
	
	public Item sell(String Name) {
		Item item = this.storage.getItem(Name);
		cash = cash + item.getPrice();
		return item;
	}
	
	public boolean buy(Item item) {
		boolean b=false;
		if(this.cash >= item.getPrice()) {
			b=true;
			this.storage.addItem(item);
			this.cash-=item.getPrice();
		}
		return b;
	}
	public void displayCash() {
		System.out.println("cash = "+ this.cash);
		
	}

}
